import argparse

def get_command_line_arguments():
    parser = argparse.ArgumentParser(description="Process optimization input")

    parser.add_argument('-y', '--yaml', '--config', type=str,
                        help="The path to the YAML file that defines the problem",
                        required=True)

    parser.add_argument('-p', '--patient', type=int, dest='patient',
                        help="Row indexes of virtual population TSV for which the therapy will be sinthesized.",
                        required=False)

    parser.add_argument('-o', '--output', type=str, dest='output',
                        help="The output JSON file pathname.",
                        required=False)

    parser.add_argument('-l', '--log', type=str, dest='log',
                        help="The optimization log file pathname.",
                        required=False)

    return parser.parse_args()


if __name__=="__main__":
    task, yaml = get_command_line_arguments()
    print(f"Task: {task}")
    print(f"YAML configuration file: {yaml}")

from apricopt.solving.whitebox.COPASI.COPASIOptimisationMethod import COPASIOptimisationMethod
from apricopt.solving.whitebox.COPASI.COPASISolverParameter import COPASISolverParameter
from apricopt.solving.whitebox.COPASI.method.COPASIGeneticAlgorithm import COPASIGeneticAlgorithm
from apricopt.solving.whitebox.COPASI.method.COPASIParticleSwarm import COPASIParticleSwarm
from apricopt.solving.whitebox.COPASI.method.COPASILevenbergMarquardt import COPASILevenbergMarquardt
from apricopt.solving.whitebox.COPASI.method.COPASINelderMead import COPASINelderMead
from apricopt.solving.whitebox.COPASI.method.COPASIRandomSearch import COPASIRandomSearch
from apricopt.model.Model import Model
from apricopt.simulation.SimulationEngine import SimulationEngine

from typing import Dict, Union, List
import json


def get_optimisation_method(optimisation_method_string: str) -> COPASIOptimisationMethod:
    if optimisation_method_string.lower() in ['genetic algorithm', 'geneticalgorithm', 'ga']:
        print("Optimisation method: Genetic Algorithm")
        return COPASIGeneticAlgorithm()
    elif optimisation_method_string.lower() in ['particle swarm', 'particleswarm', 'pm']:
        print("Optimisation method: Particle Swarm")
        return COPASIParticleSwarm()
    elif optimisation_method_string.lower() in ['levenberg-marquardt', 'levenberg marquardt', 'levenbergmarquardt', 'lm']:
        print("Optimisation method: Levenberg - Marquardt")
        return COPASILevenbergMarquardt()
    elif optimisation_method_string.lower() in ['nelder-mead', 'nelder mead', 'neldermead', 'nm']:
        print("Optimisation method: Nelder Mead")
        return COPASINelderMead()
    elif optimisation_method_string.lower() in ['random search', 'random-search', 'randomsearch', 'rs']:
        print("Optimisation method: Random Search")
        return COPASIRandomSearch()
    else:
        raise ValueError(f"The optimisation method {optimisation_method_string} is not supported.")


def get_solver_parameters(optimisation_method: COPASIOptimisationMethod,
                          input_method_parameters: Dict[str, Union[int, float]]) -> List[COPASISolverParameter]:

    method_parameters = []
    if isinstance(optimisation_method, COPASIGeneticAlgorithm):
        method_parameters = [COPASIGeneticAlgorithm.SEED,
                             COPASIGeneticAlgorithm.POPULATION_SIZE,
                             COPASIGeneticAlgorithm.NUMBER_OF_GENERATIONS,
                             COPASIGeneticAlgorithm.MUTATION_VARIANCE,
                             COPASIGeneticAlgorithm.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASIParticleSwarm):
        method_parameters = [COPASIParticleSwarm.SWARM_SIZE,
                             COPASIParticleSwarm.STD_DEVIATION,
                             COPASIParticleSwarm.ITERATION_LIMIT,
                             COPASIParticleSwarm.SEED,
                             COPASIParticleSwarm.RANDOM_NUMBER_GENERATOR]

    elif isinstance(optimisation_method, COPASILevenbergMarquardt):
        method_parameters = [COPASILevenbergMarquardt.ITERATION_LIMIT,
                             COPASILevenbergMarquardt.TOLERANCE,
                             COPASILevenbergMarquardt.MODULATION,
                             COPASILevenbergMarquardt.INITIAL_LAMBDA,
                             COPASILevenbergMarquardt.LAMBDA_INCREASE,
                             COPASILevenbergMarquardt.LAMBDA_DECREASE]

    elif isinstance(optimisation_method, COPASINelderMead):
        method_parameters = [COPASINelderMead.ITERATION_LIMIT,
                             COPASINelderMead.TOLERANCE,
                             COPASINelderMead.SCALE]

    elif isinstance(optimisation_method, COPASIRandomSearch):
        method_parameters = [COPASIRandomSearch.NUMBER_OF_ITERATIONS,
                             COPASIRandomSearch.RANDOM_NUMBER_GENERATOR,
                             COPASIRandomSearch.SEED]


    copasi_parameter_objects = []
    for param in method_parameters:
        if param in input_method_parameters.keys():
            copasi_parameter_objects.append(
                COPASISolverParameter(param, input_method_parameters[param])
            )

    return copasi_parameter_objects


def dump_synthesis_results(model: Model, sim_engine: SimulationEngine, horizon: float,
                           optimized_therapy: Dict[str, float], objective_value: float,
                           function_evaluations: int, statistics: dict, output_filename: str) -> None:

    model.set_fixed_params(optimized_therapy)
    observations = sim_engine.simulate_trajectory(model, horizon)

    data = dict()
    for obs_id, obs_val in observations.items():
        if obs_id != 'time':
            data[obs_id] = obs_val[-1]

    data['synthesized_therapy'] = optimized_therapy
    data['objective_value'] = objective_value
    data['function_evaluations'] = function_evaluations
    data['statistics'] = statistics

    with open(output_filename, 'w') as f:
        json.dump(data, f, indent=4)

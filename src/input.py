from typing import Tuple, Dict, List, Set
import pandas as pd
import json

from apricopt.model.Model import Model
from apricopt.model.Parameter import Parameter
from apricopt.model.Observable import Observable
from apricopt.simulation.SimulationEngine import SimulationEngine
from apricopt.simulation.COPASI.COPASIEngine import COPASIEngine
from apricopt.solving.whitebox.WhiteBoxSolver import WhiteBoxSolver
from apricopt.solving.whitebox.COPASI.COPASISolver import COPASISolver
from apricopt.solving.whitebox.COPASI.COPASISolverParameter import COPASISolverParameter

from apricopt.IO.data_input import parse_config_file, get_parameter_space, get_objective, \
    get_constraints_with_bounds, get_conditions

from src.utils import get_optimisation_method, get_solver_parameters


def parse_therapy_synthesis_config_file(config_filename: str, cli_args) -> \
        Tuple[Model, Dict[str, float], Dict[str, float],
              List[str],
              float, SimulationEngine, WhiteBoxSolver,
              List[COPASISolverParameter], str]:
    """
    Builds all objects needed for the personalized synthesis.
    :param config_filename: The name of the YAML file that contains the configuration
    :param cli_args: all command line parsed arguments.
    :return:
    """
    data: dict = parse_config_file(config_filename)

    sim_engine: SimulationEngine
    if data['simulator'].lower() == "copasi":
        sim_engine = COPASIEngine()
    else:
        raise ValueError("Other simulators not supported.")

    log_filename = data['optimisation_log_file'] if not cli_args.log else cli_args.log
    optimisation_method = get_optimisation_method(data['optimisation_method'])
    solver: WhiteBoxSolver
    if data['solver'].lower() == "copasi":
        solver = COPASISolver(optimisation_method=optimisation_method, log_filename=log_filename)
    else:
        raise ValueError("Other solvers not supported.")

    patient_population_index: int = int(data['patient']) if not cli_args.patient else cli_args.patient

    model: Model

    model, virtual_patient, initial_treatment, \
        treatments_to_restart, exclude_from_initialization = \
        initialise_model_with_files_therapy_synthesis(files=data['files'], sim_engine=sim_engine,
                                                       abs_tol=data['absolute_tolerance'],
                                                       rel_tol=data['relative_tolerance'],
                                                       time_step=data['time_step'],
                                                       vp_index=patient_population_index)

    random_seed = int(data['random_seed'])

    if "restarts" in data:
        restarts = int(data['restarts'])
    else:
        restarts = 0

    if isinstance(solver, COPASISolver):
        solver.set_restart_configuration(random_seed, restarts, treatments_to_restart, log_filename)
    else:
        raise ValueError("No other solvers can be supported.")

    if "solver_parameters" in data:
        solver_parameters_dict = data['solver_parameters']
        solver_parameters = get_solver_parameters(optimisation_method, solver_parameters_dict)
        print("The user-defined hyperparameters values are:")
        for param in solver_parameters:
            print(f"\t{param.id}: {param.value}")
    else:
        solver_parameters = None

    horizon = float(data['time_horizon'])
    output_filename = data['output_file'] if not cli_args.output else cli_args.output

    return model, virtual_patient, initial_treatment, \
           exclude_from_initialization, \
           horizon, sim_engine, solver, \
           solver_parameters, output_filename


def initialise_model_with_files_therapy_synthesis(files: Dict[str, str], sim_engine: SimulationEngine,
                                                    abs_tol: float,
                                                    rel_tol: float,
                                                    time_step: float,
                                                    vp_index: int):
    if 'exclude_from_initialization' in files:
        f = open(files['exclude_from_initialization'])
        exclude_text = f.read()
        f.close()
        exclude_from_initialization = json.loads(exclude_text)
    else:
        exclude_from_initialization = []

    if 'observed_outputs' in files:
        f = open(files['observed_outputs'])
        observed_outputs_text = f.read()
        f.close()
        observed_outputs = json.loads(observed_outputs_text)
    else:
        observed_outputs = None

    # The parameter space is the space of treatment parameters
    treatment_parameter_space: Set[Parameter] = get_parameter_space(read_tsv(files['treatment_parameters']))
    objective: Observable = get_objective(read_tsv(files['objective']))

    treatment_constraints: Set[Observable] = get_constraints_with_bounds(read_tsv(files['treatment_constraints']))

    virtual_patient: Dict[str, float] = get_virtual_patient(files['virtual_patients'], vp_index)

    treatments_to_restart: List[Dict[str, float]] = \
        [treat_params for treat_id, treat_params in sorted(get_conditions(read_tsv(files['initial_treatment'])).items(),
                                                           key=lambda i : i[0])]
    initial_treatment = treatments_to_restart.pop(0)
    #initial_treatments = [treat_params for treat_id, treat_params in sorted(get_conditions(read_tsv(files['initial_treatments'])).items(),
    #                                                       key=lambda i : i[0])]

    model: Model = Model(sim_engine, files['model'], abs_tol, rel_tol, time_step,
                                                     observed_outputs=observed_outputs)

    model.set_parameter_space(treatment_parameter_space)
    model.objective = objective
    model.constraints = treatment_constraints

    return model, virtual_patient, initial_treatment, \
           treatments_to_restart, exclude_from_initialization


def read_tsv(pathname: str) -> pd.DataFrame:
    frame = pd.read_csv(pathname, delimiter='\t')
    if 'parameterId' in frame.columns:
        frame.set_index('parameterId', inplace=True)
    elif 'conditionId' in frame.columns:
        frame.set_index('conditionId', inplace=True)
    elif 'observableId' in frame.columns:
        frame.set_index('observableId', inplace=True)
    return frame


def get_virtual_patient(population_path: str, vp_row_index: int) -> Dict[str, float]:
    population_frame = pd.read_csv(population_path, delimiter='\t')
    patient_row = population_frame.iloc[vp_row_index]
    patient: Dict[str, float] = dict()
    for k, v in patient_row.items():
        patient[k] = v
    return patient


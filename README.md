# Intelligent Search for Personalized Cancer Therapy Synthesis: an Experimental Comparison #

This repository contains the full source code to reproduce all experiments shown in the paper and execute additional ones.

It also contains all the figures of the paper, as well as the code to re-generate them.

### COPASI algorithms
The supported algorithms are the following:

* [Random Search](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods/Random_Search/)
* [Levenberg Marquardt](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods/Levenberg_-_Marquardt/)
* [Nelder Mead](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods/Nelder_-_Mead/)
* [Genetic Algorithm](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods/Genetic_Algorithm/)
* [Particle Swarm Optimization](http://copasi.org/Support/User_Manual/Methods/Optimization_Methods/Particle_Swarm/)


## How to run

### Requirements

Our tool depends on the following requirements:

* [Python >= 3.8](https://www.python.org/)

### Installation

To install the tool execute the following instructions:
```
cd therapy-synthesis-rcra-2021
virtualenv --system-site-packages .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Run

To run the optimization execute the following command:

```
python run.py --config=case_studies/CRC/therapy_synthesis/input/yaml/therapy_synthesis.yaml -p 0 -l /absolute/path/to/case_studies/CRC/therapy_synthesis/output/log_optimization_example.txt -o case_studies/CRC/therapy_synthesis/output/virtual_patient.json
```

* `--config`is a mandatory input argument to specify the YAML configuration file file.
* `-p` is an optional input argument to specify the index of the Virtual Patient for which the therapy is to be synthesized. If no value is provided, the tool uses the VP index inside the YAML file.
* `-l` is an optional input argument to specify the optimization log file. If not provided, the tool uses the output path inside the YAML file. (Warning: due to a bug of COPASI, you can only specify an absolute path or a relative path of this type "optimization_log.txt"). In the latter case, you will find the optimization log within the model folder.
* `-o` is an optional input argument to set the path for the output therapy. If not provided, the tool uses the path inside the YAML file.


### Experiments

The directory `case_studies/CRC/therapy_synthesis/yaml/experiments/` contains the YAML configuration files used to perform all experiments. 
To replicate an experiment you must select a YAML file and adequately set the above options (or the YAML file).


### Plots

To generate all plots run the related scripts as follows:

```
cd experiments/plots
./run_plots.sh
```

You will find all plots inside the folder: `plots/paper/`


## Authors

* [Marco Esposito](esposito@di.uniroma1.it)
* [Leonardo Picchiami](picchiami.1643888@studenti.uniroma1.it)


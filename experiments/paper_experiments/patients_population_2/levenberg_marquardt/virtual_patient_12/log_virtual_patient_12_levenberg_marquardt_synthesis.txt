======= APRICOPT =========

=== Optimization through COPASI solver ===


Solver settings: 

Method: Levenberg - Marquardt
    Log Verbosity: 0
    Iteration Limit: 10
    Tolerance: 0.001
    Modulation: 1e-06
    Stop after # stalled iterations: 0
    Initial Lambda: 1
    Lambda Decrease: 2
    Lambda Increase: 4



Optimization Log:

Function Evaluation	Objective Value		Optimization Parameters
1			15.8181			(	4	24	24	24	24	24	24	24	24	24	24	24	24	24	24	24	3	160	160	160	160	160	160	160	160	160	160	160	160	160	160	160	)

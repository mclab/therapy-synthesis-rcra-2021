=========== Therapy synthesis optimisation settings ========
Optimisation method: Levenberg - Marquardt
The user-defined hyperparameters values are:
	Iteration Limit: 10
	Tolerance: 0.001


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 0.66 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

python /mnt/nfs/lpicchiami/therapy-synthesis-rcra/run.py --config=/mnt/nfs/lpicchiami/therapy-synthesis-rcra/case_studies/CRC/therapy_synthesis/input/yaml/experiments/genetic_algorithm/therapy_synthesis_genetic_algorithm_14_patients_all_parameters.yaml -l /mnt/exps/lpicchiami/rcra-2021-treatments/patients_all_parameters/genetic_algorithm/virtual_patient_8/log_virtual_patient_8_genetic_algorithm_synthesis.txt -p 8 -o /mnt/exps/lpicchiami/rcra-2021-treatments/patients_all_parameters/genetic_algorithm/virtual_patient_8/virtual_patient_8_genetic_algorithm.json


--------------------------------------------------------------------------------


=========== Therapy synthesis optimisation settings ========
Optimisation method: Genetic Algorithm
The user-defined hyperparameters values are:
	Seed: 104
	Population Size: 64
	Number of Generations: 25


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 0.65 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...
Therapy optimisation finished..
Therapy optimisation elapsed time: 4320.83 sec


Best value found in 1577 function evaluations: 

{'cibi_m4_in': 2.33790423653366e-50, 'ate_m14_in': 6.928053719851107e-44, 'cibi_m2_in': 147.86043933821648, 'ate_m3_in': 21.714284970559387, 'cibi_m12_in': 112.21155816014935, 'cibi_m11_in': 120.3040985353708, 'cibi_m15_in': 2.853261770800616e-265, 'cibi_m9_in': 1.8143166080836475e-258, 'ate_m8_in': 18.67168077487358, 'cibi_m10_in': 138.16732571102776, 'ate_m10_in': 20.627215728717818, 'cibi_freq_in': 5.6706010133644424, 'cibi_m14_in': 0.0, 'ate_m12_in': 1.20012434919749e-265, 'cibi_m6_in': 6.835624832076089e-153, 'ate_m15_in': 16.903545148780733, 'cibi_m3_in': 5.1805410588372806e-247, 'ate_m6_in': 18.924979174530705, 'cibi_m5_in': 4.0222916894522384e-266, 'cibi_m13_in': 98.300319097408, 'ate_m2_in': 18.77264696315956, 'ate_m1_in': 14.001982299489123, 'cibi_m8_in': 102.59923151828573, 'ate_m9_in': 21.32801979868088, 'ate_freq_in': 3.913959164467059, 'cibi_m1_in': 100.9963446999308, 'ate_m13_in': 1.7654448014781554e-303, 'ate_m4_in': 24.0, 'ate_m5_in': 24.0, 'ate_m7_in': 14.629461098967564, 'cibi_m7_in': 146.0671944050304, 'ate_m11_in': 24.0}



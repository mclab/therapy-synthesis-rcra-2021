=========== Therapy synthesis optimisation settings ========
Optimisation method: Nelder Mead
The user-defined hyperparameters values are:
	Iteration Limit: 1200
	Tolerance: 0.1


============ Therapy Synthesis through COPASI Solver ===========
Virtual patient initialisation..
Virtual patient intialisation time: 0.65 sec
Initial treatment total drugs: 100.00%
Start therapy optimisation...

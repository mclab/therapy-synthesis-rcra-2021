import numpy as np
import math
import json

from lib.plot_lib import save_plot, plot_double_axis_bar_administrations
from lib.input_lib import get_command_line_arguments_experiments

from palettable.cubehelix import Cubehelix

def round_ate(a):
    if a < 0.01:
        return 0
    else:
        return a


def round_cibi(b):
    if b < 0.5:
        return 0
    else:
        return b


def round_custom(b):
    if b - math.floor(b) < 0.5:
        return math.floor(b)
    else:
        return math.ceil(b)


def time_analysis(t, *args):
    m1 = args[0]
    m2 = args[1]
    m3 = args[2]
    m4 = args[3]
    m5 = args[4]
    m6 = args[5]
    m7 = args[6]
    m8 = args[7]
    m9 = args[8]
    m10 = args[9]
    m11 = args[10]
    m12 = args[11]
    m13 = args[12]
    m14 = args[13]
    m15 = args[14]
    interval_size = 4

    if (t >= 0 and t <= interval_size * 1) and m1 > 0:
        return True

    elif (t > interval_size * 1 and t <= interval_size * 2) and m2 > 0:
        return True

    elif (t > interval_size * 2 and t <= interval_size * 3) and m3 > 0:
        return True

    elif (t > interval_size * 3 and t <= interval_size * 4) and m4 > 0:
        return True

    elif (t > interval_size * 4 and t <= interval_size * 5) and m5 > 0:
        return True

    elif (t > interval_size * 5 and t <= interval_size * 6) and m6 > 0:
        return True

    elif (t > interval_size * 6 and t <= interval_size * 7) and m7 > 0:
        return True

    elif (t > interval_size * 7 and t <= interval_size * 8) and m8 > 0:
        return True

    elif (t > interval_size * 8 and t <= interval_size * 9) and m9 > 0:
        return True

    elif (t > interval_size * 9 and t <= interval_size * 10) and m10 > 0:
        return True

    elif (t > interval_size * 10 and t <= interval_size * 11) and m11 > 0:
        return True

    elif (t > interval_size * 11 and t <= interval_size * 12) and m12 > 0:
        return True

    elif (t > interval_size * 12 and t <= interval_size * 13) and m13 > 0:
        return True

    elif (t > interval_size * 13 and t <= interval_size * 14) and m14 > 0:
        return True

    elif (t > interval_size * 14 and t <= interval_size * 15) and m15 > 0:
        return True
    else:
        return False


def time_administered(t, *args):
    m1 = args[0]
    m2 = args[1]
    m3 = args[2]
    m4 = args[3]
    m5 = args[4]
    m6 = args[5]
    m7 = args[6]
    m8 = args[7]
    m9 = args[8]
    m10 = args[9]
    m11 = args[10]
    m12 = args[11]
    m13 = args[12]
    m14 = args[13]
    m15 = args[14]
    interval_size = 4

    if (t >= 0 and t <= interval_size * 1) and m1 > 0:
        return m1

    elif (t > interval_size * 1 and t <= interval_size * 2) and m2 > 0:
        return m2

    elif (t > interval_size * 2 and t <= interval_size * 3) and m3 > 0:
        return m3

    elif (t > interval_size * 3 and t <= interval_size * 4) and m4 > 0:
        return m4

    elif (t > interval_size * 4 and t <= interval_size * 5) and m5 > 0:
        return m5

    elif (t > interval_size * 5 and t <= interval_size * 6) and m6 > 0:
        return m6

    elif (t > interval_size * 6 and t <= interval_size * 7) and m7 > 0:
        return m7

    elif (t > interval_size * 7 and t <= interval_size * 8) and m8 > 0:
        return m8

    elif (t > interval_size * 8 and t <= interval_size * 9) and m9 > 0:
        return m9

    elif (t > interval_size * 9 and t <= interval_size * 10) and m10 > 0:
        return m10

    elif (t > interval_size * 10 and t <= interval_size * 11) and m11 > 0:
        return m11

    elif (t > interval_size * 11 and t <= interval_size * 12) and m12 > 0:
        return m12

    elif (t > interval_size * 12 and t <= interval_size * 13) and m13 > 0:
        return m13

    elif (t > interval_size * 13 and t <= interval_size * 14) and m14 > 0:
        return m14

    elif (t > interval_size * 14 and t <= interval_size * 15) and m15 > 0:
        return m15
    else:
        return 0


def main():
    patient3, output = get_command_line_arguments_experiments()

    for algo in ["particle_swarm", "genetic_algorithm", 'nelder_mead', 'random_search']:
        in_path = f'{patient3}/{algo}/virtual_patient_{3}/virtual_patient_{3}_{algo}.json'
        with open(in_path, 'r') as f:
            result = json.load(f)

        therapy = result['synthesized_therapy']

        ate_freq = round_custom(therapy['ate_freq_in'])
        ate_freq_days = ate_freq * 7
        ate_m1 = round_ate(therapy['ate_m1_in'])
        ate_m2 = round_ate(therapy['ate_m2_in'])
        ate_m3 = round_ate(therapy['ate_m3_in'])
        ate_m4 = round_ate(therapy['ate_m4_in'])
        ate_m5 = round_ate(therapy['ate_m5_in'])
        ate_m6 = round_ate(therapy['ate_m6_in'])
        ate_m7 = round_ate(therapy['ate_m7_in'])
        ate_m8 = round_ate(therapy['ate_m8_in'])
        ate_m9 = round_ate(therapy['ate_m9_in'])
        ate_m10 = round_ate(therapy['ate_m10_in'])
        ate_m11 = round_ate(therapy['ate_m11_in'])
        ate_m12 = round_ate(therapy['ate_m12_in'])
        ate_m13 = round_ate(therapy['ate_m13_in'])
        ate_m14 = round_ate(therapy['ate_m14_in'])
        ate_m15 = round_ate(therapy['ate_m15_in'])

        cibi_freq = round_custom(therapy['cibi_freq_in'])
        cibi_freq_days = cibi_freq * 7
        cibi_m1 = round_cibi(therapy['cibi_m1_in'])
        cibi_m2 = round_cibi(therapy['cibi_m2_in'])
        cibi_m3 = round_cibi(therapy['cibi_m3_in'])
        cibi_m4 = round_cibi(therapy['cibi_m4_in'])
        cibi_m5 = round_cibi(therapy['cibi_m5_in'])
        cibi_m6 = round_cibi(therapy['cibi_m6_in'])
        cibi_m7 = round_cibi(therapy['cibi_m7_in'])
        cibi_m8 = round_cibi(therapy['cibi_m8_in'])
        cibi_m9 = round_cibi(therapy['cibi_m9_in'])
        cibi_m10 = round_cibi(therapy['cibi_m10_in'])
        cibi_m11 = round_cibi(therapy['cibi_m11_in'])
        cibi_m12 = round_cibi(therapy['cibi_m12_in'])
        cibi_m13 = round_cibi(therapy['cibi_m13_in'])
        cibi_m14 = round_cibi(therapy['cibi_m14_in'])
        cibi_m15 = round_cibi(therapy['cibi_m15_in'])

        horizon = 58

        time = range(0, horizon)
        time_plot = [i for i in range(0, horizon)]
        vals_ate = []
        for t in time:
            if t % ate_freq < 1 and time_analysis(t, ate_m1, ate_m2,
                                                      ate_m3, ate_m4, ate_m5,
                                                      ate_m6, ate_m7, ate_m8,
                                                      ate_m9, ate_m10, ate_m11,
                                                      ate_m12, ate_m13, ate_m14,
                                                      ate_m15):
                val = time_administered(t, ate_m1, ate_m2,
                                        ate_m3, ate_m4, ate_m5,
                                        ate_m6, ate_m7, ate_m8,
                                        ate_m9, ate_m10, ate_m11,
                                        ate_m12, ate_m13, ate_m14,
                                        ate_m15) / 24 * 100
                if val > 0 and val < 1:
                    val = 1
                vals_ate.append(val)

            else:
                vals_ate.append(0)

        ate_schedule = np.array(vals_ate)

        vals_cibi = []
        for t in time:
            if t % cibi_freq < 1 and time_analysis(t, cibi_m1, cibi_m2,
                                                    cibi_m3, cibi_m4, cibi_m5,
                                                    cibi_m6, cibi_m7, cibi_m8,
                                                    cibi_m9, cibi_m10, cibi_m11,
                                                    cibi_m12, cibi_m13, cibi_m14,
                                                    cibi_m15):
                val = time_administered(t, cibi_m1, cibi_m2,
                                        cibi_m3, cibi_m4, cibi_m5,
                                        cibi_m6, cibi_m7, cibi_m8,
                                        cibi_m9, cibi_m10, cibi_m11,
                                        cibi_m12, cibi_m13, cibi_m14,
                                        cibi_m15) / 160 * 100
                if val > 0 and val < 1:
                    val = 1
                vals_cibi.append(val)
            else:
                vals_cibi.append(0)

        cibi_schedule = np.array(vals_cibi)

        ate_administered = ate_schedule
        cibi_administered = cibi_schedule

        col = ["#0990f0", "#FD7702"]
        fig = plot_double_axis_bar_administrations(400, 1200, ['Atezolizumab', 'Cibisatamab'], time_plot,
                            [ate_administered.tolist(), cibi_administered.tolist()],
                            "% Atezolizumab", "% Cibisatamab", col, False if algo != "nelder_mead" else True)

        save_plot(fig, f"{output}/{algo}-virtual-patient-{3}.png")


if __name__ == '__main__':
    main()
import pandas as pd
import argparse as arg


def get_frame_data(csv_filename: str) -> pd.DataFrame:
    frame = pd.read_csv(csv_filename)
    return frame


def get_command_line_arguments_all_data():
    parser = arg.ArgumentParser(description="Process optimisation input for all algorithms plot")

    parser.add_argument('--genetic', type=str, dest='genetic',
                        help="The path to the CSV of Genetic Algorithm experiments",
                        required=True)

    parser.add_argument('--particle', type=str, dest='particle',
                        help="The path to the CSV of Particle Swarm experiments",
                        required=True)

    parser.add_argument('--levenberg', type=str, dest='levenberg',
                        help="The path to the CSV of Levenberg Marquardt experiments",
                        required=True)

    parser.add_argument('--nelder', type=str, dest='nelder',
                        help="The path to the CSV of Nelder Mead experiments",
                        required=True)

    parser.add_argument('--random', type=str, dest='random',
                        help="The path to the CSV of Random Search experiments",
                        required=True)

    parser.add_argument('--reference', type=str, dest='reference',
                        help="The path to the CSV of Reference Treatment experiments",
                        required=True)

    parser.add_argument('-o', '--out', type=str, dest='out',
                        help="Plot output path.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.genetic, arguments.particle, arguments.levenberg, arguments.nelder, \
           arguments.random, arguments.reference, arguments.out


def get_command_line_arguments_tumour_behaviours():
    parser = arg.ArgumentParser(description="Process optimisation input for tumour behaviours")

    parser.add_argument('--genetic', type=str, dest='genetic',
                        help="The path to the CSV of Genetic Algorithm experiments",
                        required=True)

    parser.add_argument('--particle', type=str, dest='particle',
                        help="The path to the CSV of Particle Swarm experiments",
                        required=True)

    parser.add_argument('--nelder', type=str, dest='nelder',
                        help="The path to the CSV of Nelder Mead experiments",
                        required=True)

    parser.add_argument('--random', type=str, dest='random',
                        help="The path to the CSV of Random Search experiments",
                        required=True)

    parser.add_argument('--reference', type=str, dest='reference',
                        help="The path to the CSV of Reference Treatment experiments",
                        required=True)

    parser.add_argument('--no-treatment', type=str, dest='no_treatment',
                        help="The path to the CSV of no treatment simulations",
                        required=True)

    parser.add_argument('-o', '--out-folder', type=str, dest='out_folder',
                        help="Plot output folder.",
                        required=True)

    arguments = parser.parse_args()

    return arguments.genetic, arguments.particle, arguments.nelder, \
           arguments.random, arguments.reference, arguments.no_treatment, arguments.out_folder


def get_command_line_arguments_constraints():
    parser = arg.ArgumentParser(description="Process optimisation input for the plot about constraint")

    parser.add_argument('--input', type=str, dest='input',
                        help="The path to the CSV of constraints over time",
                        required=True)

    parser.add_argument('--output-double', type=str, dest='output_double',
                        help="The path for the output plot",
                        required=True)

    parser.add_argument('--output-cumulative', type=str, dest='output_cumulative',
                        help="The path for the output ",
                        required=True)

    arguments = parser.parse_args()

    return arguments.input, arguments.output_double, arguments.output_cumulative


def get_command_line_arguments_experiments():
    parser = arg.ArgumentParser(description="Process optimisation input for the plot about constraint")

    parser.add_argument('--patient', type=str, dest='patient',
                        help="The path of",
                        required=True)

    parser.add_argument('--output', type=str, dest='output',
                        help="The path for the output plot",
                        required=True)

    arguments = parser.parse_args()

    return arguments.patient, arguments.output

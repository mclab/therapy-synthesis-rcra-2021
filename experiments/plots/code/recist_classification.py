from lib.plot_lib import save_plot, plot_grouped_bar
from lib.input_lib import get_command_line_arguments_all_data, get_frame_data

import pandas as pd
from typing import Dict

from palettable.cubehelix import Cubehelix


def get_recist_classification(init_tum: float, end_tum: float) -> str:
    recist = (end_tum - init_tum) / init_tum * 100
    if recist < -30 and end_tum < 10:
        return "CR"
    elif recist < -30 and end_tum >= 10:
        return "PR"
    elif recist > 20:
        return "PD"
    else:
        return "SD"


def calculate_recist_treatment(treatment_frame: pd.DataFrame) -> Dict[str, int]:
    recist_classification_counter = dict()
    for i in ['CR', 'PR', 'SD', 'PD']:
        recist_classification_counter[i] = 0

    for index, row in treatment_frame.iterrows():
        classification = get_recist_classification(row['initial_tumour_diameter'], row['final_tumour_diameter'])
        recist_classification_counter[classification] += 1

    return recist_classification_counter


def plot_recist_classification(plot_filename: str, genetic_counter, particle_counter, levenberg_counter,
                          nelder_counter, random_counter, reference_counter) -> None:

    optimal_genetic = [genetic_counter['CR'], genetic_counter['PR'], genetic_counter['SD'], genetic_counter['PD']]

    optimal_particle = [particle_counter['CR'], particle_counter['PR'], particle_counter['SD'], particle_counter['PD']]
    optimal_levenberg = [levenberg_counter['CR'], levenberg_counter['PR'], levenberg_counter['SD'],
                         levenberg_counter['PD']]

    optimal_nelder = [nelder_counter['CR'], nelder_counter['PR'], nelder_counter['SD'], nelder_counter['PD']]
    optimal_random = [random_counter['CR'], random_counter['PR'], random_counter['SD'], random_counter['PD']]

    reference = [reference_counter['CR'], reference_counter['PR'], reference_counter['SD'], reference_counter['PD']]

    data =  [[elem for elem in reference],
             [elem for elem in optimal_levenberg],
             [elem for elem in optimal_nelder],
             [elem for elem in optimal_random],
             [elem for elem in optimal_genetic],
             [elem for elem in optimal_particle]]

    names = ['Complete Response', 'Partial Response', 'Stable Disease', 'Progressive Disease']

    palette = Cubehelix.make(start=0.4, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]

    plot = plot_grouped_bar(800, 1900, 6,
                            ['Reference', 'Levenberg - Marquardt', 'Nelder Mead', 'Random Search', 'Genetic Algorithm',
                             'Particle Swarm'],
                            names, data, '# Patients', [
                                colors[2], colors[5], colors[7], colors[9], colors[11], colors[13]
                            ])

    save_plot(plot, plot_filename)


def main() -> None:
    genetic_path, particle_path, levenberg_path, nelder_path, \
        random_path, reference_path, out_path = get_command_line_arguments_all_data()

    # CSV loading
    optimal_frame_genetic = get_frame_data(genetic_path)
    optimal_frame_particle = get_frame_data(particle_path)
    optimal_frame_levenberg = get_frame_data(levenberg_path)
    optimal_frame_nelder = get_frame_data(nelder_path)
    optimal_frame_random = get_frame_data(random_path)
    initial_treatment_frame = get_frame_data(reference_path)

    # Recist classification computing
    recist_initial_classification = calculate_recist_treatment(initial_treatment_frame)
    recist_optimal_classification_genetic = calculate_recist_treatment(optimal_frame_genetic)
    recist_optimal_classification_particle = calculate_recist_treatment(optimal_frame_particle)
    recist_optimal_classification_levenberg = calculate_recist_treatment(optimal_frame_levenberg)
    recist_optimal_classification_nelder = calculate_recist_treatment(optimal_frame_nelder)
    recist_optimal_classification_random = calculate_recist_treatment(optimal_frame_random)


    # Plot recist classification
    plot_recist_classification(out_path, recist_optimal_classification_genetic,
                               recist_optimal_classification_particle, recist_optimal_classification_levenberg,
                               recist_optimal_classification_nelder, recist_optimal_classification_random,
                               recist_initial_classification)


if __name__ == '__main__':
    main()
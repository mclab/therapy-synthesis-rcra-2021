from lib.plot_lib import save_plot, plot_multiline_tumour_over_time
from lib.input_lib import get_frame_data, get_command_line_arguments_tumour_behaviours

import pandas as pd


def plot_tumour_behaviour_over_time(frame_data: pd.DataFrame, out_path: str, optimal: bool) -> None:
    horizon = 400
    time = [i for i in range(horizon + 1)]
    lines = []
    for column in list(frame_data.columns):
        trajectory = list(frame_data[column])
        start = trajectory[0]
        lines.append(
            [(val - start) / start * 100 for val in trajectory]
        )
    fig = plot_multiline_tumour_over_time(700, 900, len(lines), lines, time, "Months", optimal=optimal)
    save_plot(fig, out_path)


def main() -> None:
    genetic_path, particle_path, nelder_path, \
        random_path, reference_path, no_treatment_path, out_path = get_command_line_arguments_tumour_behaviours()

    genetic_tumour_frame = get_frame_data(genetic_path)
    plot_tumour_behaviour_over_time(genetic_tumour_frame, f"{out_path}/genetic-algorithm-tumour.png", True)

    particle_tumour_frame = get_frame_data(particle_path)
    plot_tumour_behaviour_over_time(particle_tumour_frame, f"{out_path}/particle-swarm-tumour.png", True)

    nelder_tumour_frame = get_frame_data(nelder_path)
    plot_tumour_behaviour_over_time(nelder_tumour_frame, f"{out_path}/nelder-mead-tumour.png", True)

    random_tumour_frame = get_frame_data(random_path)
    plot_tumour_behaviour_over_time(random_tumour_frame, f"{out_path}/random-search-tumour.png", True)

    reference_tumour_frame = get_frame_data(reference_path)
    plot_tumour_behaviour_over_time(reference_tumour_frame, f"{out_path}/initial-treatment-tumour.png", True)

    no_treatment_tumour_frame = get_frame_data(no_treatment_path)
    plot_tumour_behaviour_over_time(no_treatment_tumour_frame, f"{out_path}/tumor-untreated.png", False)



if __name__ == '__main__':
    main()
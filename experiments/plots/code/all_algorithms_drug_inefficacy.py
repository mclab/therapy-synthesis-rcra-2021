from typing import List

from lib.plot_lib import save_plot, plot_double_axis_bar
from lib.input_lib import get_frame_data, get_command_line_arguments_all_data
from statistics import mean

from palettable.cubehelix import Cubehelix



#ALL_PATIENT_NUM = 25
#PREFIX_DATA_PATH = "/home/leonardo/Scrivania/RCRA-2021-treatments/paper_experiments"
#OUTPUT_PLOT_PATH = "../data/plots/drug_inefficacy/all_algo_experiments_drug_inefficacy_all_patients.png"


def process_csv_data(initial_treatment_path, genetic_algorithm_path, particle_swarm_path,
                     levenberg_marquardt_path, nelder_mead_path, random_search_path):

    initial_treatment_frame = get_frame_data(initial_treatment_path)
    genetic_algorithm_frame = get_frame_data(genetic_algorithm_path)
    particle_swarm_frame = get_frame_data(particle_swarm_path)
    levenberg_marquardt_frame = get_frame_data(levenberg_marquardt_path)
    nelder_mead_frame = get_frame_data(nelder_mead_path)
    random_search_frame = get_frame_data(random_search_path)

    optimal_data = {'genetic_algorithm': {'total_drugs': [], 'inefficacy': []},
                    'particle_swarm': {'total_drugs': [], 'inefficacy': []},
                    'levenberg_marquardt': {'total_drugs': [], 'inefficacy': []},
                    'random_search': {'total_drugs': [], 'inefficacy': []},
                    'nelder_mead': {'total_drugs': [], 'inefficacy': []}}

    initial_data = {'total_drugs': [], 'inefficacy': []}

    for id, row in initial_treatment_frame.iterrows():
        initial_data['total_drugs'].append(row['total_drugs'])
        initial_data['inefficacy'].append(row['inefficacy'] / row['inefficacy'] * 100)

    for id, row in genetic_algorithm_frame.iterrows():
        optimal_data['genetic_algorithm']['total_drugs'].append(row['total_drugs'])
        optimal_data['genetic_algorithm']['inefficacy'].append(
                row['inefficacy'] / initial_treatment_frame.loc[id]['inefficacy'] * 100)

    for id, row in particle_swarm_frame.iterrows():
        optimal_data['particle_swarm']['total_drugs'].append(row['total_drugs'])
        optimal_data['particle_swarm']['inefficacy'].append(
                row['inefficacy'] / initial_treatment_frame.loc[id]['inefficacy'] * 100)

    for id, row in levenberg_marquardt_frame.iterrows():
        optimal_data['levenberg_marquardt']['total_drugs'].append(row['total_drugs'])
        optimal_data['levenberg_marquardt']['inefficacy'].append(
                row['inefficacy'] / initial_treatment_frame.loc[id]['inefficacy'] * 100)

    for id, row in nelder_mead_frame.iterrows():
        optimal_data['nelder_mead']['total_drugs'].append(row['total_drugs'])
        optimal_data['nelder_mead']['inefficacy'].append(
                row['inefficacy'] / initial_treatment_frame.loc[id]['inefficacy'] * 100)

    for id, row in random_search_frame.iterrows():
        optimal_data['random_search']['total_drugs'].append(row['total_drugs'])
        optimal_data['random_search']['inefficacy'].append(
                row['inefficacy'] / initial_treatment_frame.loc[id]['inefficacy'] * 100)

    return initial_data, optimal_data


def plot_double_axis(plot_filename: str, drugs: List[float], kpis: List[float]) -> None:
    data = [drugs, kpis]
    
    names = ['Reference', 'Levenberg - Marquardt', 'Nelder Mead', 'Random Search', 'Genetic Algorithm',
             'Particle Swarm']
    
    palette = Cubehelix.make(start=0.3, rotation=-0.5, n=16)
    colors = [f"rgb({c[0]},{c[1]},{c[2]})" for c in palette.colors]
    plot = plot_double_axis_bar(850, 2200, ['Avg Drugs Amount %', 'Avg Inefficacy %'],
                                names, data, 'Avg Drugs Amount %',
                                'Avg Inefficacy %', bar_colors=[colors[9], colors[5]], bar_text_size=28)
    
    save_plot(plot, plot_filename)
    
    
def main():
    genetic_path, particle_path, levenberg_path, nelder_path, \
        random_path, reference_path, out_path = get_command_line_arguments_all_data()

    initial_data, optimal_data = process_csv_data(reference_path, genetic_path, particle_path,
                                                  levenberg_path, nelder_path, random_path)


    data_drugs = [mean(initial_data['total_drugs']),
                  mean(optimal_data['levenberg_marquardt']['total_drugs']),
                  mean(optimal_data['nelder_mead']['total_drugs']),
                  mean(optimal_data['random_search']['total_drugs']),
                  mean(optimal_data['genetic_algorithm']['total_drugs']),
                  mean(optimal_data['particle_swarm']['total_drugs'])]

    inefficacy = [mean(initial_data['inefficacy']),
                  mean(optimal_data['levenberg_marquardt']['inefficacy']),
                  mean(optimal_data['nelder_mead']['inefficacy']),
                  mean(optimal_data['random_search']['inefficacy']),
                  mean(optimal_data['genetic_algorithm']['inefficacy']),
                  mean(optimal_data['particle_swarm']['inefficacy'])]

    plot_double_axis(out_path, data_drugs, inefficacy)


if __name__ == '__main__':
    main()

    

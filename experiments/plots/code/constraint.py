from lib.input_lib import get_frame_data, get_command_line_arguments_constraints
from lib.plot_lib import plot_line, plot_multiline, save_plot


def main():
    input_path, output_double, output_cumulative = get_command_line_arguments_constraints()
    constraint_data_frame = get_frame_data(input_path)

    horinzon = 100
    time = [i for i in range(horinzon + 1)]

    # ATE and ATE PREVIOUS
    data = [list(constraint_data_frame['ate_total_drug_time']), list(constraint_data_frame['ate_total_drug_previous_time'])]
    fig = plot_multiline(700, 700, 2, ["Atezolizumab", "28-day-shifted Atezolizumab"], data, time, "Time (days)",
                         "")
    save_plot(fig, output_double)

    # CUMULATIVE CONSTRAINT
    data_cum = list(constraint_data_frame['ate_cumulative_constraint'])
    fig_cum = plot_line(700, 700, data_cum, time, "Time  (days)", "")
    save_plot(fig_cum, output_cumulative)



if __name__ == '__main__':
    main()
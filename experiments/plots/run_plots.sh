#!/bin/bash

cd code || exit

echo "Generating plot: drug-inefficacy all algorithms..."
python all_algorithms_drug_inefficacy.py --genetic ../data/treatments/genetic_algorithm_optimal_treatments.csv \
      --particle ../data/treatments/particle_swarm_optimal_treatments.csv \
      --levenberg ../data/treatments/levenberg_marquardt_optimal_treatments.csv \
      --nelder ../data/treatments/nelder_mead_optimal_treatments.csv \
      --random ../data/treatments/random_search_optimal_treatments.csv \
      --reference ../data/treatments/initial_treatment_all_patients.csv \
      --out ../figures/algorithms/all-algo-experiments-drug-inefficacy.png

echo "Generating plot: RECIST..."
python recist_classification.py --genetic ../data/treatments/genetic_algorithm_optimal_treatments.csv \
      --particle ../data/treatments/particle_swarm_optimal_treatments.csv \
      --levenberg ../data/treatments/levenberg_marquardt_optimal_treatments.csv \
      --nelder ../data/treatments/nelder_mead_optimal_treatments.csv \
      --random ../data/treatments/random_search_optimal_treatments.csv \
      --reference ../data/treatments/initial_treatment_all_patients.csv \
      --out ../figures/recist/recist-all-algo.png

echo "Generating plot: tumour behaviour over time..."
python tumour_over_time.py --genetic ../data/tumours/genetic_algorithm_tumour_all_patients.csv \
      --particle ../data/tumours/particle_swarm_tumour_all_patients.csv \
      --nelder ../data/tumours/nelder_mead_tumour_all_patients.csv \
      --random ../data/tumours/random_search_tumour_all_patients.csv \
      --reference ../data/tumours/initial_treatment_tumour_all_patients.csv \
      --no-treatment ../data/tumours/no_treatment_tumour_all_patients.csv \
      --out-folder ../figures/tumours

echo "Generating plot: constraints..."
python constraint.py --input ../data/constraint/ate_constraint.csv \
       --output-double ../figures/constraint/ate-quantity-in-period.png \
       --output-cumulative ../figures/constraint/ate-cumulative-constraint.png

echo "Generating plot: administrations over time..."
python treatments_administration.py --patient ../../paper_experiments/patients_population_1 \
       --output ../figures/administrations
